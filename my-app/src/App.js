import logo from './logo.svg';
import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";
import React, { Component } from "react";

export default class App extends Component {
    state = {
        isFirstModalOpen: false,
        isSecondModalOpen: false,
    }
    openFirstModal() {
        this.setState({isModalFirstOpen: true})
    }

    openSecondModal() {
        this.setState({isModalSecondOpen: true});
    }

    closeModal(e){
        this.setState({isModalFirstOpen: false});
        this.setState({isModalSecondOpen: false});
    }
    render() {
        const {isModalFirstOpen, isModalSecondOpen} = this.state;
        return (
            <div className='App'>
                <div className='btn-container'>
                    <Button
                        text='open first modal'
                        backgroundColor='darkred'
                        onClick={
                            ()=>this.openFirstModal()
                        }
                    />
                    <Button
                        text='open second modal'
                        backgroundColor='teal'
                        onClick={
                            ()=>this.openSecondModal()
                        }
                    />
                </div>

                {
                    isModalFirstOpen &&
                    <Modal
                        backgroundColor='crimson'
                        backgroundHeaderColor='firebrick'
                        backgroundCloseBtnColor='firebrick'
                        header={"Do you want to delete this file?"}
                        closeButton={true}
                        text={'Once your delete this file, it will not be possible to undo this action. Are you sure you want to delete it?'}
                        actions={
                            <div>
                                <Button
                                    onClick={(e) => this.closeModal(e)}
                                    text="Ok"
                                    className="button"
                                    backgroundColor='darkred'
                                />
                                <Button
                                    onClick={(e) => this.closeModal(e)}
                                    text="Cancel"
                                    className="button"
                                    backgroundColor='darkred'
                                />
                            </div>
                        }
                        onClick={(e) => this.closeModal(e)}
                    >
                    </Modal>
                }

                {
                    isModalSecondOpen &&
                    <Modal
                        backgroundColor='cadetblue'
                        backgroundHeaderColor='darkcyan'
                        backgroundCloseBtnColor='darkcyan'
                        header={"Do you want to edit this file?"}
                        closeButton={true}
                        text={'Make your choice, please'}
                        actions={
                        <div>
                            <Button
                                onClick={(e) => this.closeModal(e)}
                                text="Edit"
                                className="button"
                                backgroundColor='teal'
                            />
                            <Button
                                onClick={(e) => this.closeModal(e)}
                                text="Cancel"
                                className="button"
                                backgroundColor='teal'
                            />
                        </div>
                    }
                        onClick={(e) => this.closeModal(e)}
                        >
                    </Modal>
                }

            </div>
        )
    }}
// function App() {
//   return (
//     <div className="App">
//         <Button
//             text='open first modal'
//             backgroundColor='red'>
//             onClick={
//             ()=>this.openFirstModal()
//         }
//         </Button>
//     </div>
//   );
// }

// export default App;
