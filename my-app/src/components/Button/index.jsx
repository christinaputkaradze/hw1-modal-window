import React from "react";
import "./style.scss"

 class Button extends React.Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;
        return (
            <button
                style={{ backgroundColor: backgroundColor }}
                className="button"
                onClick={onClick}
            >
                {text}
            </button>

        );
    }
}

export default Button;



