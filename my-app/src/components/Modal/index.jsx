import React from "react";
import "./style.scss";

class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.modalRef = React.createRef();
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleOutsideClick);
    }

    handleOutsideClick(event) {
        if (this.modalRef.current && !this.modalRef.current.contains(event.target)) {
            this.props.onClick();
        }
    }

    render() {
        const { header, closeButton, text, actions, onClick, backgroundColor, backgroundHeaderColor, backgroundCloseBtnColor } = this.props;
        return (
            <>
                <div className="modal-wrapper">
                    <div ref={this.modalRef} className="modal-container"
                         style={{ backgroundColor: backgroundColor }}>
                        <div className="modal">
                            <div className="modal-header"
                                 style={{ backgroundColor: backgroundHeaderColor }}>
                                <h2 className="modal-title">{header}</h2>
                                {closeButton && <button
                                    style={{ backgroundColor: backgroundCloseBtnColor }}
                                    className={'modal-close'}
                                    onClick={onClick}>
                                  Х
                                 </button>}
                            </div>
                            <div className="modal-content">
                                <p className="modal-text">{text}</p>
                                <div className="modal-btns">{actions}</div>
                            </div>
                        </div>
                     </div>
                </div>
            </>

        );
    }
}

export default Modal;


